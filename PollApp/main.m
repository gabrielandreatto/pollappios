//
//  main.m
//  PollApp
//
//  Created by PollApp on 11/25/13.
//  Copyright (c) 2013 PollApp. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "PAAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([PAAppDelegate class]));
    }
}
