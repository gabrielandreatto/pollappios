//
//  PAModalViewController.m
//  PollApp
//
//  Created by Indigo on 11/26/13.
//  Copyright (c) 2013 PollApp. All rights reserved.
//

#import "PAModalViewControllerBase.h"

@interface PAModalViewControllerBase ()

@end

@implementation PAModalViewControllerBase

- (id)initWithTitle:(NSString *)title
{
    self = [super init];
    if (self) {
        self.title = title;
    }
    return self;
}

- (void)loadView
{
    self.view = [[UIScrollView  alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.view setBackgroundColor:[UIColor whiteColor]];
    
    // TODO: change this button to removeButton (remove image)
    UIButton *closeButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    closeButton.frame = CGRectMake(0, 0, 50, 30);
    [closeButton setTitle:@"Close" forState:UIControlStateNormal];
    [closeButton setTitle:@"Close" forState:UIControlStateHighlighted];
    
    [closeButton addTarget:self action:@selector(closeModal:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *closeButtonItem = [[UIBarButtonItem alloc] initWithCustomView:closeButton];
    
    self.navigationItem.leftBarButtonItem = closeButtonItem;
}

- (void)closeModal:(UIButton *)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)setRightButton:(UIButton *)rightButton
{
    UIBarButtonItem *rightButtonItem = [[UIBarButtonItem alloc] initWithCustomView:rightButton];
    self.navigationItem.rightBarButtonItem = rightButtonItem;
}

@end
