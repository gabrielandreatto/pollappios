//
//  UILabel+CustomLabels.m
//  PollApp
//
//  Created by Indigo on 11/26/13.
//  Copyright (c) 2013 PollApp. All rights reserved.
//

#import "UILabel+CustomLabels.h"

@implementation UILabel (CustomLabels)

+ (UILabel *)headingOne
{
    UILabel *headingOneLabel = [[UILabel alloc] init];
    
    headingOneLabel.font = [UIFont fontWithName:@"OpenSans-Bold" size:12.0];
    headingOneLabel.textColor = [UIColor paletteBlueOne];

    return headingOneLabel;
}

+ (UILabel *)headingTwo
{
    UILabel *headingTwoLabel = [[UILabel alloc] init];
    
    headingTwoLabel.font = [UIFont fontWithName:@"OpenSans-Bold" size:12.0];
    headingTwoLabel.textColor = [UIColor paletteBlack];
    
    return headingTwoLabel;
}

+ (UILabel *)paragraph
{
    UILabel *headingOneLabel = [[UILabel alloc] init];
    
    headingOneLabel.font = [UIFont fontWithName:@"OpenSans" size:12];
    headingOneLabel.textColor = [UIColor paletteBlack];
    
    return headingOneLabel;
}

@end
