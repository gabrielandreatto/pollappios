//
//  PAViewController.h
//  PollApp
//
//  Created by PollApp on 11/25/13.
//  Copyright (c) 2013 PollApp. All rights reserved.
//

#import <UIKit/UIKit.h>

#define kPAScrollPadding 40

@interface PAExamplesController : UIViewController

@property (strong, nonatomic) UIScrollView *contentSubview;
@property (strong, nonatomic) NSNumber* currentHeight;

- (void)updateScrollView:(UIView *)addedView;

@end
