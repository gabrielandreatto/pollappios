//
//  PAViewController.m
//  PollApp
//
//  Created by PollApp on 11/25/13.
//  Copyright (c) 2013 PollApp. All rights reserved.
//

#import "UIViewController+PAViewController.h"

#import "PAExamplesController.h"
#import "UIColor+ColorPalette.h"
#import "UILabel+CustomLabels.h"
#import "PAExamples2Controller.h"
#import "PAModalViewControllerBase.h"
#import "PANavigationController.h"

@interface PAExamplesController ()

@end

@implementation PAExamplesController


- (id)init
{
    self = [super init];
    if(self) {
        self.title = @"Home";
    }
    return self;
}

- (void)loadView
{
    self.view = [[UIScrollView  alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    self.contentSubview =[[UIScrollView alloc] init];
    
    [self.view addSubview:self.contentSubview];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self setPaletteLabel];
    [self setWhiteButton];
    [self setBlueOneButton];
    [self setBlueTwoButton];
    [self setBlueThreeButton];
    [self setBlueFourButton];
    [self setGrayOneButton];
    [self setGrayTwoButton];
    [self setGrayThreeButton];
    [self setGrayFourButton];
    [self setGrayFiveButton];
    

    [self setFontsLabel];
    [self setHeadingOne];
    [self setHeadingTwo];
    [self setParagraph];
    
    
    [self setHeaderExampleLabel];
    [self setHeaderButton];
    [self setHeaderExampleButton];
    
    
    [self setTextViewLabel];

}

- (void)didReceiveMemoryWarning 
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    
    
    self.contentSubview.contentInset = UIEdgeInsetsMake(self.topLayoutGuide.length, 0, self.bottomLayoutGuide.length, 0);
    self.contentSubview.contentOffset = CGPointMake(0, - self.topLayoutGuide.length);
    self.contentSubview.frame = CGRectMake(0,
                                           0,
                                           CGRectGetWidth(self.view.frame),
                                           CGRectGetHeight(self.view.frame));
}

/* Custom methods */

- (void)updateScrollView:(UIView *)addedView
{
    self.contentSubview.contentSize = CGSizeMake(CGRectGetWidth(self.view.frame),
                                                 CGRectGetMaxY(addedView.frame) + kPAScrollPadding);
    NSNumber *lastY = @(CGRectGetMaxY(addedView.frame));
    if(lastY > self.currentHeight)
        self.currentHeight = @(CGRectGetMaxY(addedView.frame));
}



// COLOR PALETTE
- (void)setPaletteLabel
{
    UILabel *paletteLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.view.frame), 30)];
    paletteLabel.text = @"Color Palette";
    [paletteLabel setTextColor:[UIColor paletteGrayOne]];
    [paletteLabel setBackgroundColor:[UIColor paletteGrayFive]];
    [paletteLabel setTextAlignment:NSTextAlignmentCenter];
    
    [self.contentSubview addSubview:paletteLabel];
}

// set buttons
- (void)setWhiteButton
{
    [self addButtonWithRect:CGRectMake(10, 40, 75, 25)
                      title:@"White"
                     action:@selector(whiteButtonPressed:)];
}

- (void)setBlueOneButton
{
    [self addButtonWithRect:CGRectMake(85, 40, 75, 25)
                      title:@"BlueOne"
                     action:@selector(blueOneButtonPressed:)];
}

- (void)setBlueTwoButton
{
    [self addButtonWithRect:CGRectMake(160, 40, 75, 25)
                      title:@"BlueTwo"
                     action:@selector(blueTwoButtonPressed:)];
}

- (void)setBlueThreeButton
{
    [self addButtonWithRect:CGRectMake(235, 40, 75, 25)
                      title:@"BlueThree"
                     action:@selector(blueThreeButtonPressed:)];
}

- (void)setBlueFourButton
{
    [self addButtonWithRect:CGRectMake(10, 70, 75, 25)
                      title:@"BlueFour"
                     action:@selector(blueFourButtonPressed:)];
}

- (void)setGrayOneButton
{
    [self addButtonWithRect:CGRectMake(85, 70, 75, 25)
                      title:@"GrayOne"
                     action:@selector(grayOneButtonPressed:)];
}

- (void)setGrayTwoButton
{
    [self addButtonWithRect:CGRectMake(160, 70, 75, 25)
                      title:@"GrayTwo"
                     action:@selector(grayTwoButtonPressed:)];
}

- (void)setGrayThreeButton
{
    [self addButtonWithRect:CGRectMake(235, 70, 75, 25)
                      title:@"GrayThree"
                     action:@selector(grayThreeButtonPressed:)];
}

- (void)setGrayFourButton
{
    [self addButtonWithRect:CGRectMake(10, 100, 75, 25)
                      title:@"GrayFour"
                     action:@selector(grayFourButtonPressed:)];
}

- (void)setGrayFiveButton
{
    [self addButtonWithRect:CGRectMake(85, 100, 75, 25)
                      title:@"GrayFive"
                     action:@selector(grayFiveButtonPressed:)];
}

- (void)addButtonWithRect:(CGRect)buttonRect title:buttonTitle action:(SEL)action
{
    UIButton *blueOneButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    blueOneButton.frame = buttonRect;
    [blueOneButton setTitle:buttonTitle forState:UIControlStateNormal];
    [blueOneButton setTitle:buttonTitle forState:UIControlStateHighlighted];
    
    [blueOneButton addTarget:self action:action forControlEvents:UIControlEventTouchUpInside];
    
    [self.contentSubview addSubview:blueOneButton];
    [self updateScrollView:blueOneButton];
}


// listeners
- (void)whiteButtonPressed:(UIButton *)sender
{
    [self.view setBackgroundColor:[UIColor whiteColor]];
}

- (void)blueOneButtonPressed:(UIButton *)sender
{
    [self.view setBackgroundColor:[UIColor paletteBlueOne]];
}

- (void)blueTwoButtonPressed:(UIButton *)sender
{
    [self.view setBackgroundColor:[UIColor paletteBlueTwo]];
}

- (void)blueThreeButtonPressed:(UIButton *)sender
{
    [self.view setBackgroundColor:[UIColor paletteBlueThree]];
}

- (void)blueFourButtonPressed:(UIButton *)sender
{
    [self.view setBackgroundColor:[UIColor paletteBlueFour]];
}

- (void)grayOneButtonPressed:(UIButton *)sender
{
    [self.view setBackgroundColor:[UIColor paletteGrayOne]];
}

- (void)grayTwoButtonPressed:(UIButton *)sender
{
    [self.view setBackgroundColor:[UIColor paletteGrayTwo]];
}

- (void)grayThreeButtonPressed:(UIButton *)sender
{
    [self.view setBackgroundColor:[UIColor paletteGrayThree]];
}

- (void)grayFourButtonPressed:(UIButton *)sender
{
    [self.view setBackgroundColor:[UIColor paletteGrayFour]];
}

- (void)grayFiveButtonPressed:(UIButton *)sender
{
    [self.view setBackgroundColor:[UIColor paletteGrayFive]];
}
// END COLOR PALETTE



// FONTS/LABELS
- (void)setFontsLabel
{
    UILabel *fontsLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, [self.currentHeight floatValue] + kPAScrollPadding, CGRectGetWidth(self.view.frame), 30)];
    fontsLabel.text = @"Labels/Fonts";
    [fontsLabel setTextColor:[UIColor paletteGrayOne]];
    [fontsLabel setBackgroundColor:[UIColor paletteGrayFive]];
    [fontsLabel setTextAlignment:NSTextAlignmentCenter];
    
    [self.contentSubview addSubview:fontsLabel];
    [self updateScrollView:fontsLabel];
}

- (void)setHeadingOne
{
    UILabel *headingOneLabel = [UILabel headingOne];
    [headingOneLabel setFrame:CGRectMake(10, [self.currentHeight floatValue], CGRectGetWidth(self.view.frame), 30)];
    
    headingOneLabel.text = @"Heading One";
    [self.contentSubview addSubview:headingOneLabel];
    [self updateScrollView:headingOneLabel];
}

- (void)setHeadingTwo
{
    UILabel *headingOneLabel = [UILabel headingTwo];
    [headingOneLabel setFrame:CGRectMake(10, [self.currentHeight floatValue], CGRectGetWidth(self.view.frame), 30)];
    
    headingOneLabel.text = @"Heading Two";
    [self.contentSubview addSubview:headingOneLabel];
    [self updateScrollView:headingOneLabel];
}

- (void)setParagraph
{
    UILabel *headingOneLabel = [UILabel paragraph];
    [headingOneLabel setFrame:CGRectMake(10, [self.currentHeight floatValue], CGRectGetWidth(self.view.frame), 30)];
    
    headingOneLabel.text = @"Paragraph";
    [self.contentSubview addSubview:headingOneLabel];
    [self updateScrollView:headingOneLabel];
}
// END FONTS/LABELS



// HEADER SETTINGS
- (void)setHeaderExampleLabel
{
    UILabel *fontsLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, [self.currentHeight floatValue] + kPAScrollPadding, CGRectGetWidth(self.view.frame), 30)];
    fontsLabel.text = @"Header example";
    [fontsLabel setTextColor:[UIColor paletteGrayOne]];
    [fontsLabel setBackgroundColor:[UIColor paletteGrayFive]];
    [fontsLabel setTextAlignment:NSTextAlignmentCenter];
    
    [self.contentSubview addSubview:fontsLabel];
    [self updateScrollView:fontsLabel];
}

- (void)setHeaderButton
{
    UIButton *addPollButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    addPollButton.frame = CGRectMake(0, 0, 50, 30);
    [addPollButton setTitle:@"Poll+" forState:UIControlStateNormal];
    [addPollButton setTitle:@"Poll+" forState:UIControlStateHighlighted];
    
    [addPollButton addTarget:self action:@selector(modalExemple:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *newPollButton = [[UIBarButtonItem alloc] initWithCustomView:addPollButton];
    self.navigationItem.rightBarButtonItem = newPollButton;
}

- (void)setHeaderExampleButton
{
    [self addButtonWithRect:CGRectMake(10, [self.currentHeight floatValue] + 10, 140, 25)
                      title:@"Next example page"
                     action:@selector(nextExampleControllerButtonPressed:)];
}

- (void)nextExampleControllerButtonPressed:(UIButton *)sender
{
    PAExamples2Controller *examples2VC = [[PAExamples2Controller alloc] init];
    [self.navigationController pushViewController:examples2VC animated:YES];
}
// END HEADER SETTINGS


// TEXT VIEW
- (void)setTextViewLabel
{
    UILabel *fontsLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, [self.currentHeight floatValue] + kPAScrollPadding, CGRectGetWidth(self.view.frame), 30)];
    fontsLabel.text = @"TextView";
    [fontsLabel setTextColor:[UIColor paletteGrayOne]];
    [fontsLabel setBackgroundColor:[UIColor paletteGrayFive]];
    [fontsLabel setTextAlignment:NSTextAlignmentCenter];
    
    [self.contentSubview addSubview:fontsLabel];
    [self updateScrollView:fontsLabel];
}
// END TEXT VIEW


// MODAL EXAMPLE
- (void)modalExemple: (UIButton *)sender
{
    // In your implementation, don't use the PAModalViewControllerBase class. Instead, extends from it and use your own class.
    PAModalViewControllerBase *sampleView = [[PAModalViewControllerBase alloc] initWithTitle:@"Modal"];
    
    UIButton *rightButtonButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    rightButtonButton.frame = CGRectMake(0, 0, 50, 30);
    [rightButtonButton setTitle:@"Right" forState:UIControlStateNormal];
    [rightButtonButton setTitle:@"Right" forState:UIControlStateHighlighted];
    
    [sampleView setRightButton:rightButtonButton];
    
    [self openModal:sampleView];
}
// END MODAL EXAMPLE


/* END Custom methods */

@end
