//
//  PAAppDelegate.h
//  PollApp
//
//  Created by PollApp on 11/25/13.
//  Copyright (c) 2013 PollApp. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIColor+ColorPalette.h"
#import "PAExamplesController.h"
#import "PAExamples2Controller.h"
#import "PANavigationController.h"

@class PAExamplesController;

@interface PAAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) PAExamplesController *examplesVC;

@end
