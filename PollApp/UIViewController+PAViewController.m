//
//  UIViewController+PAViewController.m
//  PollApp
//
//  Created by Indigo on 11/26/13.
//  Copyright (c) 2013 PollApp. All rights reserved.
//

#import "UIViewController+PAViewController.h"

#import "PANavigationController.h"

@implementation UIViewController (PAViewController)

- (void)openModal:(UIViewController *)viewController
{
    PANavigationController *modalNavController =
    [[PANavigationController alloc] initWithRootViewController:viewController];
    
    [self presentViewController:modalNavController animated:YES completion:nil];
}

@end
