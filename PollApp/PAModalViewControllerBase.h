//
//  PAModalViewController.h
//  PollApp
//
//  Created by Indigo on 11/26/13.
//  Copyright (c) 2013 PollApp. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PAModalViewControllerBase : UIViewController

- (id)initWithTitle:(NSString *)title;
- (void)setRightButton:(UIButton *)rightButton;

@end
