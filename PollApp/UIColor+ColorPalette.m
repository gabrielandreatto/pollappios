//
//  UIColor+ColorPalette.m
//  PollApp
//
//  Created by PollApp on 11/25/13.
//  Copyright (c) 2013 PollApp. All rights reserved.
//

#import "UIColor+ColorPalette.h"

@implementation UIColor (ColorPalette)


// Blues (good music)
+ (UIColor *)paletteBlueOne
{
    return [UIColor colorWithRed:0.149 green:0.318 blue:0.502 alpha:1];
}

+ (UIColor *)paletteBlueTwo
{
    return [UIColor colorWithRed:0.0 green:0.243 blue:0.502 alpha:1];
}

+ (UIColor *)paletteBlueThree
{
    return [UIColor colorWithRed:0.0 green:0.435 blue:0.902 alpha:1];
}

+ (UIColor *)paletteBlueFour
{
    return [UIColor colorWithRed:0.749 green:0.871 blue:1.0 alpha:1];
}

// 50 shades of gray (good book)
+ (UIColor *)paletteBlack
{
    return [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:1];
}

+ (UIColor *)paletteGrayOne
{
    return [UIColor colorWithRed:0.2 green:0.2 blue:0.2 alpha:1];
}

+ (UIColor *)paletteGrayTwo
{
    return [UIColor colorWithRed:0.431 green:0.431 blue:0.431 alpha:1];
}

+ (UIColor *)paletteGrayThree
{
    return [UIColor colorWithRed:0.667 green:0.667 blue:0.667 alpha:1];
}

+ (UIColor *)paletteGrayFour
{
    return [UIColor colorWithRed:0.839 green:0.839 blue:0.839 alpha:1];
}

+ (UIColor *)paletteGrayFive
{
    return [UIColor colorWithRed:0.914 green:0.914 blue:0.914 alpha:1];
}

@end
