//
//  UIViewController+PAViewController.h
//  PollApp
//
//  Created by Indigo on 11/26/13.
//  Copyright (c) 2013 PollApp. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (PAViewController)

- (void)openModal:(UIViewController *)viewController;

@end
