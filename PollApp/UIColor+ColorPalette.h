//
//  UIColor+ColorPalette.h
//  PollApp
//
//  Created by PollApp on 11/25/13.
//  Copyright (c) 2013 PollApp. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (ColorPalette)

+ (UIColor *)paletteBlueOne;
+ (UIColor *)paletteBlueTwo;
+ (UIColor *)paletteBlueThree;
+ (UIColor *)paletteBlueFour;

+ (UIColor *)paletteBlack;
+ (UIColor *)paletteGrayOne;
+ (UIColor *)paletteGrayTwo;
+ (UIColor *)paletteGrayThree;
+ (UIColor *)paletteGrayFour;
+ (UIColor *)paletteGrayFive;

@end
